# README #

This is where you will find all my standalone PowerShell scripts. Anything that is part of a greater process will be located with that process.

Note: Before you can run any of these scripts you will first need to run "Admin_AllowSelfSignedPS.bat". This file is located here as well. This is making it so that you can run self signed PowerShell Scripts.